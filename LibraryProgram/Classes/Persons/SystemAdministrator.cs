﻿using static Globals;

internal class SystemAdministrator : Person

{
    public SystemAdministrator(string name, string workExperience, string phoneNumber) : base(name, PersonStatus.SystemAdministrator, phoneNumber) {
    WorkExperience= workExperience;
    }
    public string WorkExperience { get; private set; }
}

