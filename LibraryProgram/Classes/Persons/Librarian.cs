﻿using static Globals;

internal class Librarian:Person
{
    public Librarian(string name, string workDays, string phoneNumber) : base(name, PersonStatus.Librarian, phoneNumber) { 
    WorkDays= workDays;
    }

    public string WorkDays { get; private set; }
}

