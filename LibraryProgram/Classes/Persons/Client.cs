﻿using System.Net.NetworkInformation;
using static Globals;
internal class Client : Person
{
    public Client(string name, string booksCount, string phoneNumber) : base(name, PersonStatus.Client, phoneNumber) { 
        BooksCount = booksCount;
    }

    public string BooksCount { get; private set; } 
}

