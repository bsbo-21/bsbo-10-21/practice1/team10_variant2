﻿using static Globals;

internal class Manager : Person
{
    public Manager(string name, string education, string phoneNumber) : base(name, PersonStatus.Manager, phoneNumber) { 
    Education= education;
    }

    public string Education { get; private set; } 
}

