﻿using static Globals;
abstract class Person
{
    protected Person(string name, PersonStatus status, string phoneNumber)
    {
        Name = name;
        Status = status;
        PhoneNumber = phoneNumber;
    }

    public string Name { get; private set; }
    public string PhoneNumber { get; private set; }
    public PersonStatus Status { get; private set; }


}

