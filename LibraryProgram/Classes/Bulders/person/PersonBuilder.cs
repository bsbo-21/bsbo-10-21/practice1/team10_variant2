﻿internal abstract class PersonBuilder
{
    public abstract Person Build();
}

