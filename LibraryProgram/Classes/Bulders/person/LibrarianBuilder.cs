﻿using System;
internal class LibrarianBuilder : PersonBuilder
{
    public (string name, string workDays, string phoneNumber) TakeInfo()
    {
        return ("Александр", "ПН, СР, ЧТ, СБ", "3857435");
    }
    public override Person Build()
    {
        Console.WriteLine("LibrarianBuilder: Build()");
        var info = TakeInfo();
        return new Librarian(info.name, info.workDays, info.phoneNumber);
    }
}


