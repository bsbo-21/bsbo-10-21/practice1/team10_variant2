﻿using System;
internal class ClientBuilder : PersonBuilder
{
    public (string name, string booksCount, string phoneNumber) TakeInfo()
    {
        return ("Дмитрий", "12 книг", "281984" );
    }
    public override Person Build()
    {
        Console.WriteLine("ClientBuilder: Build()");
        var info = TakeInfo();
        return new Client(info.name, info.booksCount, info.phoneNumber);
    }
}

