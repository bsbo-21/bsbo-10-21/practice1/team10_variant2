﻿using System;
internal class ManagerBuilder : PersonBuilder
{
    public (string name, string education, string phoneNumber) TakeInfo()
    {
        return ("Екатерина", "Высшее техническое - РТУ МИРЭА", "456456");
    }
    public override Person Build()
    {
        Console.WriteLine("ManagerBuilder: Build()");
        var info = TakeInfo();
        return new Manager(info.name, info.education, info.phoneNumber);
    }
}

