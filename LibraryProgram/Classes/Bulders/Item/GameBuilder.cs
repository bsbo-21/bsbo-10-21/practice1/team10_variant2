﻿using System;
internal class GameBuilder : TakenItemBuilder
{
    public (string title, Globals.ItemStatus status, Globals.GameGenre genre) TakeInfo()
    {
        return ("Монополия", Globals.ItemStatus.InStock, Globals.GameGenre.Strategy);
    }
    public override TakenItem Build()
    {
        Console.WriteLine("GameBuilder: Build()");
        var info = TakeInfo();
        return new Game(info.title, info.status, info.genre);
    }
}

