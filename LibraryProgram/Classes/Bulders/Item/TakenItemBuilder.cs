﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Dynamic;
using System;
abstract class TakenItemBuilder
{
    public abstract TakenItem Build();
}

