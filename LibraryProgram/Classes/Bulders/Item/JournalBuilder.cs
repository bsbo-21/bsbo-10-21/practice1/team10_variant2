﻿using System;
internal class JournalBuilder : TakenItemBuilder
{
    public (string title, Globals.ItemStatus status, Globals.JournalTheme theme) TakeInfo()
    {
        return ("Юный шахматист", Globals.ItemStatus.InStock, Globals.JournalTheme.Chess);
    }
    public override TakenItem Build()
    {
        Console.WriteLine("JournalBuilder: Build()");
        var info = TakeInfo();
        return new Journal(info.title, info.status, info.theme);
    }
}

