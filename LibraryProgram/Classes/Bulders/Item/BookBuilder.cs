﻿using System;
internal class BookBuilder : TakenItemBuilder
{
    public (string title, Globals.ItemStatus status, string auhtor, Globals.BookGenre genre) TakeInfo()
    {
        return ("Евгений Oнегин", Globals.ItemStatus.InStock, "A.C.Пушкин", Globals.BookGenre.Novel);
    }
    public override TakenItem Build()
    {
        Console.WriteLine("BookBuilder: Build()");
        var info = TakeInfo();
        return  new Book(info.title, info.status, info.auhtor, info.genre);
    }
}

