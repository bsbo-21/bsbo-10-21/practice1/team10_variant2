﻿using System.Collections.Generic;
using static Globals;
using System;

internal static class ItemDatabase
{ 
    private static List<TakenItem> _takenItemList = new List<TakenItem>();

    private static Dictionary<ItemType, TakenItemBuilder> _takenItemBuilders = new Dictionary<ItemType, TakenItemBuilder>()
    {
        { Globals.ItemType.Journal, new JournalBuilder()},
        { Globals.ItemType.Book, new BookBuilder() },
        { Globals.ItemType.Game , new GameBuilder()}

    };
    public static void Print()
    {
        foreach (var item in _takenItemList)
        {
            System.Console.WriteLine($"Название: {item.Title}, Статус: {item.Status}, Владелец: {(item.Customer is not null ? item.Customer.Name : "не указан")} ");
        }
    }
    public static (string answer, bool flag) AddTakenItem(ItemType itemType)
    {
        Console.WriteLine("ItemDatabase: AddTakenItem()");
        _takenItemList.Add(_takenItemBuilders[itemType].Build());
        return ("Предмет успешно добавлен в базу данных", true);
    }
    public static (string answer, bool flag) RemoveTakenItem(string title)
    {
        Console.WriteLine("ItemDatabase: RemoveTakenItem()");
        foreach (var item in _takenItemList)
        {
            if (item.Title == title)
            {
                _takenItemList.Remove(item);
                return ("Предмет успешно удален из базы данных", true);
            }
        }
        return ("Удаляемый предмет не найден в базе данных", false);
    }



    public static (string answer, bool flag) CheckItem(string title)
    {
        Console.WriteLine("ItemDatabase: CheckItem()");
        foreach (var item in _takenItemList)
        {
            if ((item.Title == title)&&(item.Status is ItemStatus.InStock))
            {
                return ("Запрашиваемый предмет есть в наличии", true);
            }
        }
        return ("Запрашиваемого предмета нет в наличии", false);
    }
    public static (string answer, bool flag) ReserveItem(string title, Person person)
    {
        Console.WriteLine("ItemDatabase: ReserveItem()");
        foreach (var item in _takenItemList)
        {
            if ((item.Title == title)&&(item.Status is ItemStatus.InStock))
            {
                if (person is Client)
                {
                    item.ReserveItem(person);
                    return ("Предмет успешно зарезервирован", true);
                }
            }
        }
        return ("Предмет нельзя зарезервировать", false);
    }

    public static (string answer, bool flag) IssueItem(string title, string phoneNumber)
    {
        Console.WriteLine("ItemDatabase: IssueItem()");
        foreach (var item in _takenItemList)
        {
            if ((item.Status is ItemStatus.InStock or ItemStatus.OnReservation)&&(item.Title == title))
            {
                Person person = PersonDatabase.TakePersonAtNumber(phoneNumber);
                if (person is Client) 
                {
                    if (item.Status == ItemStatus.OnReservation)
                    {
                        if (item.Customer == person)
                        {
                            item.TakeItem(person);
                            return ("Предмет успешно выдан", true);
                        }
                    }
                    else if (item.Status == ItemStatus.InStock)
                    {
                        item.TakeItem(person);
                        return ("Предмет успешно выдан", true);
                    }
                }
            }
        }
        return ("Предмет не может быть выдан", false);
    }
    public static (string answer, bool flag) ReturnItem(string title, string phoneNumber)
    {
        Console.WriteLine("ItemDatabase: ReturnItem()");
        Person client = PersonDatabase.TakePersonAtNumber(phoneNumber);
        foreach (var item in _takenItemList)
        {
            if((item.Title == title)&&(item.Status == ItemStatus.Taken))
            {
                if (item.Customer == client)
                {
                    item.ReturnItem();
                    return ("Предмет успешно возвращен", true);
                }
            }
        }
        return ("Предмет не возвращен", false);
    }

}
