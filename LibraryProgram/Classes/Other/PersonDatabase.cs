﻿using static Globals;
using System.Collections.Generic;
using System.Threading;
using System;

internal static class PersonDatabase
{
    private static List<Person> _personList = new List<Person>();

    private static Dictionary<PersonStatus, PersonBuilder> _personBuilders = new Dictionary<PersonStatus, PersonBuilder>()
    {
        { PersonStatus.Client, new ClientBuilder()},
        { PersonStatus.Librarian, new LibrarianBuilder()},
        { PersonStatus.Manager, new ManagerBuilder()},
        { PersonStatus.SystemAdministrator, new SystemAdministratorBuilder()}


    };
    public static Person Аuthorization(int id)
    {
        Console.WriteLine("PersonDatabase: Authorization()");
        return _personList[id];
    }
    public static Person TakePersonAtNumber(string number)
    {
        Console.WriteLine("PersonDatabase: TakePersonNumber()");
        foreach (var item in _personList)
        {
            if(item.PhoneNumber == number)
            {
                return item;
            } 
            
        }
        return null;
    }
    public static void Print()
    {
        foreach (var item in _personList)
        {
            System.Console.WriteLine(item.Name);

        }
    }
    public static (string answer, bool flag) AddPerson(PersonStatus person)
    {
        Console.WriteLine("PersonDatabase: AddPerson()");
        _personList.Add(_personBuilders[person].Build());
        return ("Профиль успешно добавлен", true);
    }
    public static (string answer, bool flag) RemovePerson(string name)
    {
        Console.WriteLine("PersonDatabase: RemovePerson()");
        foreach (var item in _personList)
        {
            if (item.Name == name)
            {
                _personList.Remove(item);
                return ("Профиль успешно удален", true);
            }
        }
        return ("Профиль не найден", false);
    }
}

