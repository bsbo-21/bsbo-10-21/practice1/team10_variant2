﻿using System;
using static Globals;
internal class Program
{
    static void Main(string[] args)
    {
        // Создание пользователей с заглушкой (данные вводятся автоматически), тк нет визуального интерфейса

        Person CurrentUser;
        PersonDatabase.AddPerson(PersonStatus.SystemAdministrator);  // Создание админа
        //-----------------------------------Работа программы-----------------------------------------------
        Console.WriteLine("--------------------------");
        CurrentUser = RequestManager.Аuthorization(0); // Авторизация под Админом
        Console.WriteLine($"Авторизация: {CurrentUser.Name} {CurrentUser.Status}");
        Console.WriteLine("--------------------------");

        RequestManager.AddPerson(PersonStatus.Manager, CurrentUser); // Создание аккаунта Менеджера
        Console.WriteLine("--------------------------");
        RequestManager.AddPerson(PersonStatus.Librarian, CurrentUser); // Создание аккаунта Библиотекаря
        Console.WriteLine("--------------------------");
        RequestManager.AddPerson(PersonStatus.Client, CurrentUser); // Создание аккаунта Клиента
        Console.WriteLine("--------------------------");

        CurrentUser = RequestManager.Аuthorization(1); // Авторизация под Менеджером
        Console.WriteLine("--------------------------");
        Console.WriteLine($"Авторизация: {CurrentUser.Name} {CurrentUser.Status}");
        Console.WriteLine("--------------------------");

        Console.WriteLine(RequestManager.AddTakenItem(ItemType.Book, CurrentUser));  // Добавление книги в базу данных
        Console.WriteLine("--------------------------");

        CurrentUser = RequestManager.Аuthorization(3); // Авторизация под Клиентом
        Console.WriteLine("--------------------------");
        Console.WriteLine($"Авторизация: {CurrentUser.Name} {CurrentUser.Status}");
        Console.WriteLine("--------------------------");

        Console.WriteLine(RequestManager.CheckItem("Евгений Oнегин", CurrentUser)); // Проверка наличия книги
        Console.WriteLine("--------------------------");

        Console.WriteLine(RequestManager.ReserveItem("Евгений Oнегин", CurrentUser)); // Резервирование книги клиентом
        Console.WriteLine("--------------------------");

        CurrentUser = RequestManager.Аuthorization(2); // Авторизация под Библиотекарем
        Console.WriteLine("--------------------------");
        Console.WriteLine($"Авторизация: {CurrentUser.Name} {CurrentUser.Status}");
        Console.WriteLine("--------------------------");

        Console.WriteLine(RequestManager.IssueItem("Евгений Oнегин", "281984", CurrentUser)); // Оформление выдачи книги
        Console.WriteLine("--------------------------");

        Console.WriteLine(RequestManager.ReturnItem("Евгений Oнегин", "281984", CurrentUser)); // Оформление возврата книги
        Console.WriteLine("--------------------------");

        // ------------------------------Работа при некорректных данных------------------------------------------

        CurrentUser = RequestManager.Аuthorization(1); // Авторизация под Менеджером
        Console.WriteLine("--------------------------");
        Console.WriteLine($"Авторизация: {CurrentUser.Name} {CurrentUser.Status}");
        Console.WriteLine("--------------------------");
        Console.WriteLine(RequestManager.IssueItem("Евгений Oнегин", "281984", CurrentUser)); // Оформление выдачи книги, ожидаемый результат: "У данного пользователя нет доступа к запросу на выдачу предмета"
        Console.WriteLine("--------------------------");

        CurrentUser = RequestManager.Аuthorization(2); // Авторизация под Библиотекарем
        Console.WriteLine("--------------------------");
        Console.WriteLine($"Авторизация: {CurrentUser.Name} {CurrentUser.Status}");
        Console.WriteLine("--------------------------");
        Console.WriteLine(RequestManager.AddTakenItem(ItemType.Book, CurrentUser));  // Добавление книги в базу данных, ожидаемый результат "У данного пользователя нет доступа к запросу на выдачу предмета"
        Console.WriteLine("--------------------------");

        CurrentUser = RequestManager.Аuthorization(3); // Авторизация под Клиентом
        Console.WriteLine("--------------------------");
        Console.WriteLine($"Авторизация: {CurrentUser.Name} {CurrentUser.Status}");
        Console.WriteLine("--------------------------");
        Console.WriteLine(RequestManager.CheckItem("Белеберда", CurrentUser)); // Проверка наличия книги, ожидаемый результат "Запрашиваемого предмета нет в наличии"
        Console.WriteLine("--------------------------");

    }
 }

