﻿using System;
using System.CodeDom;
using static Globals;

internal static class RequestManager
{

    public static Person Аuthorization(int id)
    {
        Console.WriteLine("RequestManager: Authorization()");
        return PersonDatabase.Аuthorization(id);
    }

    public static (string answer, bool flag) AddPerson(PersonStatus person, Person accessPerson)
    {
        Console.WriteLine("RequestManager: AddPerson()");
        if (accessPerson.Status is PersonStatus.SystemAdministrator)
        {
            return PersonDatabase.AddPerson(person);
        }
        else
        {
            return ("У данного пользователя нет доступа к запросу на добавление новых пользователей", false);
        }
    }
  
    public static (string answer, bool flag) RemovePerson(string name, Person accessPerson)
    {
        Console.WriteLine("RequestManager: RemovePerson()");
        if (accessPerson.Status is PersonStatus.SystemAdministrator)
        {
            return PersonDatabase.RemovePerson(name);
        }
        else
        {
            return ("У данного пользователя нет доступа к запросу на удаление пользователей", false);
        }
    }
    public static (string answer, bool flag) AddTakenItem(ItemType itemType, Person accessPerson)
    {
        Console.WriteLine("RequestManager: AddTakenItem()");
        if (accessPerson.Status is PersonStatus.Manager)
        {
            return ItemDatabase.AddTakenItem(itemType);
        }
        else
        {
            return ("У данного пользователя нет доступа к запросу на добавление новых предметов в базу данных", false);
        }
    }
    public static (string answer, bool flag) RemoveTakenItem(string title, Person accessPerson)
    {
        Console.WriteLine("RequestManager: RemoveTakenItem()");
        if (accessPerson.Status is PersonStatus.Manager)
        {
            return ItemDatabase.RemoveTakenItem(title);
        }
        else
        {
            return ("У данного пользователя нет доступа к запросу на удаление предметов из базы данных", false);
        }
    }
    public static (string answer, bool flag) CheckItem(string title, Person accessPerson)
    {
        Console.WriteLine("RequestManager: CheckItem()");
        if (accessPerson.Status is PersonStatus.Client or PersonStatus.Librarian)
        {
            return ItemDatabase.CheckItem(title);
        }
        else
        {
            return ("У данного пользователя нет доступа к запросу на проверку наличия предмета", false);
        }
    }
    public static (string answer, bool flag) ReserveItem(string title, Person accessPerson)
    {
        Console.WriteLine("RequestManager: ReserveItem()");
        if (accessPerson.Status is PersonStatus.Client)
        {
            return ItemDatabase.ReserveItem(title, accessPerson); 
        }
        else
        {
            return ("У данного пользователя нет доступа к запросу на резервацию предмета", false);
        }
    }
    public static (string answer, bool flag) IssueItem(string title, string phoneNumber, Person accessPerson)
    {
        Console.WriteLine("RequestManager: IssueItem()");
        if (accessPerson.Status is PersonStatus.Librarian)
        {
            return ItemDatabase.IssueItem(title, phoneNumber);
        }
        else
        {
            return ("У данного пользователя нет доступа к запросу на выдачу предмета", false);
        }
    }
     public static (string answer, bool flag) ReturnItem(string title, string phoneNumber, Person accessPerson)
     {
        Console.WriteLine("RequestManager: ReturnItem()");
        if (accessPerson.Status is PersonStatus.Librarian)
        {
            return ItemDatabase.ReturnItem(title, phoneNumber);
        }
        else
        {
            return ("У данного пользователя нет доступа к запросу на возвращение предмета", false);
        }
    }
}

