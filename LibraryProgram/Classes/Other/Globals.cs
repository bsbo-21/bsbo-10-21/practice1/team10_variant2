﻿internal class Globals
{
    public enum ItemStatus 
    {
        InStock,
        Taken,
        OnReservation
    }
    public enum GameGenre
    {
        Strategy,
        Arcade,
        Adventures
    }
    public enum BookGenre
    {
        Novel,
        FairyTale,
        Detective
    }
    public enum JournalTheme
    {
        Scientific,
        Poetic,
        Chess
    }
    public enum ItemType
    {
        Book,
        Game,
        Journal
    }
    public enum PersonStatus
    {
        Client,
        Librarian,
        Manager,
        SystemAdministrator
    }
}


