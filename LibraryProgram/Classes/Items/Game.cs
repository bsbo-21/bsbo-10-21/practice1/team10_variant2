﻿internal class Game : TakenItem
{
    public Game(string title, Globals.ItemStatus status, Globals.GameGenre genre) : base(title, status)
    {
        Genre = genre;
    }
    public Globals.GameGenre Genre { get; private set; }
}

