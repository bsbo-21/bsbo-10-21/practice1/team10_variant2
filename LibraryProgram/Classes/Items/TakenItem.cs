﻿using System;
using System.Security.Authentication;

abstract class TakenItem
{
    protected TakenItem(string title, Globals.ItemStatus status)
    {
        Title = title;
        Status = status;
    }
    public string Title { get; private set; }
    public Globals.ItemStatus Status { get; private set; }
    public Person Customer { get; private set; } = null;
    

    public void ReturnItem()
    {
        Console.WriteLine("TakenItem: ReturnItem()");
        Customer = null;
        Status = Globals.ItemStatus.InStock;
    }
    public void ReserveItem(Person customer)
    {
        Console.WriteLine("TakenItem: ReserveItem()");
        Customer = customer;
        Status = Globals.ItemStatus.OnReservation;
    }
    public void TakeItem(Person customer)
    {
        Console.WriteLine("TakenItem: TakeItem()");
        Customer = customer;
        Status = Globals.ItemStatus.Taken;
    }


    
    
}

