﻿internal class Journal : TakenItem
{
    public Journal(string title, Globals.ItemStatus status, Globals.JournalTheme theme) : base(title, status)
    {
        Theme = theme;
    }
    public Globals.JournalTheme Theme { get; private set; }
}

