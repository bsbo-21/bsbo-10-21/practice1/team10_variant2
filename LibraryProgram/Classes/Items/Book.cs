﻿internal class Book : TakenItem
{
    public Book(string title, Globals.ItemStatus status, string auhtor, Globals.BookGenre genre) : base(title, status)
    {
        Auhtor = auhtor;
        Genre = genre;
    }

    public string Auhtor { get; private set; }
    public Globals.BookGenre Genre { get; private set; }
}

